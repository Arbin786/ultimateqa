package Configuration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesFile {

	static Properties prop = new Properties();

	// Get the project path
	static String projectPath = System.getProperty("user.dir");

	public static void main(String[] args) {

		readProperties();

		writeInPropertiesFiles();

		readProperties();

	}

	public static void readProperties()

	{
		try {

			// InputStream input = new FileInputStream(projectPath +
			// "/src/test/java/Configuration/Config.properties");
			InputStream input = new FileInputStream(
					"C:\\Users\\ARBIN\\eclipse-workspace\\UltimateQAProject\\src\\test\\java\\Configuration\\Configuration.Properties");

			prop.load(input);

			// 4. get the values from the properties file
			String browser = prop.getProperty("broswer");
			// Printing the check if correct value was received
			System.out.println(browser + " was Invoked");

			// Set the browser for the particular

			UltimateQAConfigTestNG.browserName = browser;
			System.out.println(UltimateQAConfigTestNG.browserName);

		}

		catch (IOException e) {

			System.out.println(e.getMessage());
			System.out.println(e.getCause());

			e.printStackTrace();
		}

	}

	public static void writeInPropertiesFiles()

	{
		try {

			// set data to properties file
			// Create object of class output stream
			// create properties object
			// OutputStream output = new FileOutputStream(projectPath +
			// "/src/test/java/Configuration/Config.properties");
			OutputStream output = new FileOutputStream("C:\\Users\\ARBIN\\eclipse-workspace\\"
					+ "UltimateQAProject\\src\\test\\java\\Configuration\\Configuration.Properties");
			// Setting the Values

			prop.setProperty("broswer", "Firefox");

			prop.setProperty("Result", "Pass");

			// Storing value in the properties file

			prop.store(output, "Browser Value Stored");

		}

		catch (IOException e) {

			System.out.println(e.getMessage());
			System.out.println(e.getCause());

			e.printStackTrace();

		}

	}

}
