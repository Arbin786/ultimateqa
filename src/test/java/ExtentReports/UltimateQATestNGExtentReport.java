package ExtentReports;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import Pages.AutoExeBigPageTabClassObject;
import Pages.FillOutFormClassObject;
import Pages.InteractionsElementsClassObject;
import Pages.LandingPageClassObject;
import Pages.LearnHowToAutomateClassObject;
import Pages.LoginAutomationClassObject;
import Pages.PricingPageClassObject;
import Pages.UltimateQaHomePageClassObject;

public class UltimateQATestNGExtentReport {

	// Global Variables
		ExtentHtmlReporter htmlReporter;
		static ExtentReports extent;
		ExtentTest test;
		static WebDriver driver;

		
	
	
	  @BeforeSuite
	 /* 
	 * public void beforeSuit()
	 * 
	 * {
	 * 
	 * System.out.println("Before Suite method"); }
	 */
	  
	public void setUpExtent() {
		// creating ExtentReporter object and creating new extentReport Html file
		// creating ExtentReports and attach the reporter(s)
		htmlReporter = new ExtentHtmlReporter("UltimateQA Website.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

	}

	@BeforeTest

	public void setUptest() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public static void navigateToUltimateQA() {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.UltimateQA.com/");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		ExtentTest BrowserOpened = extent.createTest("Invoke Browser",
				"This websites ensures that the browser is invoked");
		BrowserOpened.pass("Browser was invoked as Expected");

	}

	@Test(priority = 0)
	public void UltimateQAPage() throws InterruptedException {
		// navigate to ultimateqa.com

		navigateToUltimateQA();

		ExtentTest test = extent.createTest("UltimateQA Homepage", "This Page ensures it is launched");
		test.log(Status.INFO, "Homepage Launched");
		
		ExtentTest test1 = extent.createTest("UltimateQA Homepage", "This Page ensures it is launched");
		test1.log(Status.INFO, "Homepage Launched");

		// Creating an object for UltimateQaHomePage & Selecting AutomationExercisesTab

		UltimateQaHomePageClassObject obj1 = new UltimateQaHomePageClassObject(driver);
		obj1.AutomationExercisesTab();
		test1.pass("Click Automation Exercise Tab");

	}

	@Test(priority = 1)
	public void ULoginAutomationPage() throws InterruptedException {
		ExtentTest test2 = extent.createTest("Automation Exercise Page",
				"This Page ensures Automation Practice Page is Opened");
		test2.log(Status.INFO, "Automation Practice Page Launched");

		// Creating an object for LoginAutomationClassObject,

		LoginAutomationClassObject obj2 = new LoginAutomationClassObject(driver);

		obj2.validatingAutomationPracticePage();
		test2.pass("Validating Automation Practice Page");
		obj2.LoginAutomationTab();
		test2.pass("Login Automation Tab");
		obj2.validatingSignInPage();
		test2.pass("Validating SignIn Page");
		obj2.CreateNewAccount();
		test2.pass("Create a New Account Page");
		obj2.fillOutNewAccInfo();
		test2.pass("Fill Out New Account Info");
		obj2.logIn("Arbinpradhan@gmail.com", "LoveisBlind2020");
		test2.pass("LogIn ");
	}
			
			
	@Test(priority = 2)
	public void AutoExeBigPage() throws InterruptedException 
	{
		ExtentTest test3 = extent.createTest("Big Page with many Elements Page",
				"This Page ensures - Skills Improved - Page is Opened");
		test3.log(Status.INFO, "Skills Improved Page Launched");
				
		// Creating an object for AutoExeBigPageTab,

		AutoExeBigPageTabClassObject obj3 = new AutoExeBigPageTabClassObject(driver);

		obj3.BigPageManyElementTab();
		test3.pass("Big Page Many Element Tab");
		obj3.validatingBigPageManyEle();
		test3.pass("Validating Big Page Many Element Page");
		obj3.faceBookLnk();
		test3.pass("FaceBook Link");
		obj3.RandomStuffNameEmail("Binod Shrestha", "Arbinpradhan@gmail.com");
		test3.pass("Section of Random Stuff Name & Email");
		obj3.RandomStuffMessage("How Many Month QA Class will Run  ");
		test3.pass("Random Stuff Message");
		obj3.RandomStuffSumBox();
		test3.pass("Random Stuff Sum Box");
	}
			
			
	@Test(priority = 3)
	public void LandingPage() throws InterruptedException 
	{
		ExtentTest test4 = extent.createTest("Landing Page",
				"This Page ensures - View Courses - Page is Opened");
		test4.log(Status.INFO, "View Courses Page Launched");		
		
		
		// Creating an object for LandingPageClassObject,

		LandingPageClassObject obj4 = new LandingPageClassObject(driver);

		obj4.LandingPageTab();
		test4.pass("Landing Page Tab");
		obj4.validatingLandingPage();
		test4.pass("Validating View Courses Page");
		obj4.scrollPage();
		test4.pass("Scroll Page");
		
		
	}
			
	@Test(priority = 4)
	public void PricingPage() throws InterruptedException 
	{
		ExtentTest test5 = extent.createTest("Pricing Page", "This Page ensures - Pick a Plan - Page is Opened");
		test5.log(Status.INFO, "Pick a Plan Page Launched");

		// Creating an object for LandingPageClassObject,

		PricingPageClassObject obj5 = new PricingPageClassObject(driver);

		obj5.PricingPageTab();
		test5.pass("Pricing Page Tab");
		obj5.validatingPricingPage();
		test5.pass("Validating Pricing Page");
		obj5.scrollPage();
		test5.pass("Scroll Page");
	}
		
	@Test(priority = 5)
	public void FillOutFormPage() throws InterruptedException 
	
	{
		ExtentTest test6 = extent.createTest("Fill Out Forms Page", "This Page ensures - Fill Out Forms - Page is Opened");
		test6.log(Status.INFO, "Fill Out Forms Page Launched");
		
		
		
		
		
		// Creating an object for FillOutFormClassObject

		FillOutFormClassObject obj6 = new FillOutFormClassObject(driver);

		obj6.fillOutFormsPageTab();
		test6.pass("Fill Out Forms Tab");
		obj6.fillOutFormsName1("Ryan Magar", "I am looking of QA  trainging, Please call me my number is 454-584-5855");
		test6.pass("Fill Out Forms Name1 & Message1");
		obj6.fillOutFormsName2("Maya Devi", "What is the Duration of QA Class.");
		test6.pass("Fill Out Forms Name2 & Message2");
		 obj6.fillOutFormsNumber();
	}
	
	@Test(priority = 6)
	public void LearnHowToAutomatePage() throws InterruptedException
	{
		ExtentTest test7 = extent.createTest("Learn how to automate an application", "This Page ensures - Application Lifecycle - Page is Opened");
		test7.log(Status.INFO, "Application Lifecycle Page Launched");
		
		
		
		// Creating an object for LearnHowToAutomateClassObject

		LearnHowToAutomateClassObject obj7 = new LearnHowToAutomateClassObject(driver);

		obj7.learnHowToAutomateTab();
		test7.pass("Learn How To Automate Tab");
		obj7.validatinglearnHowToAutomatePage();
		test7.pass("Validating learn How To Automate Page");
		obj7.Sprint1("Harry");
		test7.pass("Enter FName Sprint1");
		obj7.GoToSprint2Iteration2("Bob", "Marley");
		test7.pass("FName & LName entered in  Sprint2");
		obj7.GoToSprint3Iteration3("", "Female", "");
		test7.pass("Gender Selected");
		obj7.GoToSprint3Iteration3FullName("Nabin", "Dangol");
		test7.pass("FName & LName enterted in Sprint3");
		obj7.GoToSprint4Iteration4("", "Female", "");
		test7.pass("Gender Selected");
		obj7.GoToSprint4Iteration4FullName("Sam", "Kumar");
		test7.pass("FName & LName enterted in Sprint 4");
		obj7.Sprint4Itert4EME("", "", "Other");
		test7.pass("Gender Selected");
		obj7.Sprint4Itert4EMEFullName("Arun", "Prasad");
		test7.pass("FName & LName enterted for Emergency Contact");

	}

	@Test(priority = 7)
	public void InteractionsElementsPage() throws InterruptedException 
	{
		ExtentTest test8 = extent.createTest("Interactions with simple elements Page", "This Page ensures - Simple Elements - Page is Opened");
		test8.log(Status.INFO, "Interactions with simple elements Page Launched");
		
		
		
		
		// Creating an object for InteractionsElementsClassObject

		InteractionsElementsClassObject obj8 = new InteractionsElementsClassObject(driver);

		obj8.InteractionsElementsTab();
		test8.pass("Interactions Elements Tab");
		obj8.validatingInteractionsElementsPage();
		test8.pass("Validating Interactions Elements Page");
		obj8.clickButtons();
		test8.pass("Click ClickButtonBox ");
		obj8.genderRadioButtons("Male", "", "");
		test8.pass("Gender Selected and Validated ");
		obj8.vehicleCheckBoxs("Bike", "");
		test8.pass("Vehicle Selected and Validated ");
		obj8.vehicleMake("Saab");
		test8.pass("Vehicle Model Selected and Validated ");

	}
	
	
	@AfterTest

	public static void terminateTest() throws InterruptedException 
	{
		ExtentTest test9 = extent.createTest("UltimateQA Website ","This page is closed");
        test9.log(Status.INFO, "Terminate the Browser");


		// Close the browser
		Thread.sleep(10000);
		 driver.close();
		 driver.quit();
		 System.out.println(" Browser Closed");

	}


	@AfterSuite	
	public void tearDown() 
	{
       extent.flush();
	}

			
			
			
	
	
}
