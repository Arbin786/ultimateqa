package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutoExeBigPageTabClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By BigPageManyElement = By.xpath("//a[contains(text(),'Big page with many elements')]");
	By validatingBigPage = By.xpath("//span[@id='Skills_Improved']");
	By facebooklink = By.xpath("(//a[contains(@title,'Follow on Facebook')])[3]");
	By RandomStfName = By.xpath("//input[@id='et_pb_contact_name_0']");
	By RandomStfEmail = By.xpath("//input[@id='et_pb_contact_email_0']");
	By RandomStfMess = By.xpath("//textarea[@id='et_pb_contact_message_0']");
	By RandomStfSum = By.xpath("//input[@name='et_pb_contact_captcha_0']");
	By capchaQuestion = By.xpath("//div[@id='et_pb_contact_form_0']//div[@class='et_contact_bottom_container']//span");
	By RandomStfSubmit = By.xpath(
			"//div[@id='et_pb_contact_form_0']//button[@name='et_builder_submit_button'][contains(text(),'Submit')]");
	By fillOutSuccess1 = By.xpath("//p[contains(text(),'Thanks for contacting us')]");
	By navigateBrowser = By.xpath("//ul[@id='top-menu']//a[contains(text(),'Automation Exercises')]");

	public AutoExeBigPageTabClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void BigPageManyElementTab() throws InterruptedException

	{

		// driver.manage().timeouts().pageLoadTimeout(15, SECONDS);

		driver.findElement(BigPageManyElement).click();
		TimeUnit.SECONDS.sleep(6);
		ScreenShotClassObject.captureScreenShots(driver, "Automation Practice Page");

	}

	public void validatingBigPageManyEle() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(validatingBigPage, "Skills Improved:"));

		WebElement validate = driver.findElement(validatingBigPage);

		if (validate.getText().equalsIgnoreCase("Skills Improved:")) {
			System.out.println("PASS--Expected Message ---HOORAY'Skills Improved: Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Skills Improved: Page Did not displayed  ");

		}

	}

	public void faceBookLnk() throws InterruptedException

	{

		// driver.manage().timeouts().pageLoadTimeout(15, SECONDS);

		driver.findElement(facebooklink).click();
		TimeUnit.SECONDS.sleep(6);
		driver.navigate().back();

	}

	public void RandomStuffNameEmail(String name, String email) throws InterruptedException

	{

		TimeUnit.SECONDS.sleep(3);
		driver.findElement(RandomStfName).clear();
		driver.findElement(RandomStfName).sendKeys(name);
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(RandomStfEmail).clear();
		driver.findElement(RandomStfEmail).sendKeys(email);
		TimeUnit.SECONDS.sleep(3);

	}

	public void RandomStuffMessage(String Mess) throws InterruptedException {
		driver.findElement(RandomStfMess).clear();
		driver.findElement(RandomStfMess).sendKeys(Mess);
		TimeUnit.SECONDS.sleep(3);

	}

	public void RandomStuffSumBox() throws InterruptedException {
		// Getting the text from page and generating sum
		String numValue1 = driver.findElement(capchaQuestion).getText().trim();
		String removeSpace = numValue1.replaceAll("\\s+", "");

		// Get two number

		String[] parts = removeSpace.split("\\+");
		String firstPart = parts[0];
		String secondPart = parts[1];
		String[] parts1 = secondPart.split("\\=");
		String lastPart = parts1[0];
		int summation = Integer.parseInt(firstPart) + Integer.parseInt(secondPart);

		String s = String.valueOf(summation);
		driver.findElement(RandomStfSum).sendKeys(s);
		System.out.println("Values are :  " + s);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(RandomStfSubmit).click();
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(navigateBrowser).click();

	}

}
