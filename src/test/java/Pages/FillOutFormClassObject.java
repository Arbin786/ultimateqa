package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class FillOutFormClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By fillOutFormsPage = By.xpath("//a[contains(text(),'Fill out forms')]");

	By fillOutName1 = By.xpath("//input[@id='et_pb_contact_name_0']");

	By fillOutMessage1 = By.xpath("//textarea[@id='et_pb_contact_message_0']");

	By fillOutSubmit1 = By.xpath("//div[@id='et_pb_contact_form_0']//button[@name='et_builder_submit_button'][contains(text(),'Submit')]");

	By fillOutName2 = By.xpath("//input[@id='et_pb_contact_name_1']");

	By fillOutMessage2 = By.xpath("//textarea[@id='et_pb_contact_message_1']");

	By fillOutNumber = By.xpath("//input[@name='et_pb_contact_captcha_1']");

	By fillOutSubmit2 = By.xpath("//button[contains(@name,'et_builder_submit_button')]");

	By fillOutSuccess1 = By.xpath("//p[contains(text(),'Form filled out successfully')]");

	By fillOutSuccess2 = By.xpath("//p[contains(text(),'Success')]");

	By capchavalues = By.xpath("//input[@name='et_pb_contact_captcha_1']");

	By capchaQuestion = By.xpath("//span[@class='et_pb_contact_captcha_question']");

	public FillOutFormClassObject(WebDriver driver1) {

		this.driver = driver1;
	}

	public void fillOutFormsPageTab() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(fillOutFormsPage).click();
		TimeUnit.SECONDS.sleep(3);

	}

	public void fillOutFormsName1(String Name1, String Message1) throws InterruptedException {
		ScreenShotClassObject.captureScreenShots(driver, "fillOutFormsPage ");

		driver.findElement(fillOutName1).clear();
		driver.findElement(fillOutName1).sendKeys(Name1);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(fillOutMessage1).clear();
		driver.findElement(fillOutMessage1).sendKeys(Message1);
		driver.findElement(fillOutSubmit1).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);		

		WebElement validate = driver.findElement(fillOutSuccess1);

		if (validate.getText().contains("Form filled out successfully")) {
			System.out.println(
					"PASS--Expected Message ---HOORAY'Form filled out successfully'-- is displayed as expected");

		}

		else {
			System.out.println("  Form did not  filled out  displayed  ");

		}

	}

	public void fillOutFormsName2(String Name2, String Message2) throws InterruptedException {

		driver.findElement(fillOutName2).clear();
		driver.findElement(fillOutName2).sendKeys(Name2);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(fillOutMessage2).clear();
		driver.findElement(fillOutMessage2).sendKeys(Message2);
		TimeUnit.SECONDS.sleep(4);

	}

	public void fillOutFormsNumber() throws InterruptedException {

		// Getting the text from page and generating sum
		String numValue1 = driver.findElement(capchaQuestion).getText().trim();
		String removeSpace = numValue1.replaceAll("\\s+", "");

		// Get two number

		String[] parts = removeSpace.split("\\+");
		String firstPart = parts[0];
		String secondPart = parts[1];
		String[] parts1 = secondPart.split("\\=");
		String lastPart = parts1[0];
		int summation = Integer.parseInt(firstPart) + Integer.parseInt(secondPart);

		String s = String.valueOf(summation);
		driver.findElement(capchavalues).sendKeys(s);
		System.out.println("Values are :  " + s);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(fillOutSubmit2).click();
		TimeUnit.SECONDS.sleep(2);
		
		WebElement validates = driver.findElement(fillOutSuccess2);

		if (validates.getText().contains("Success")) {
			System.out.println(
					"PASS--Expected Message ---'Success Message'-- is displayed as expected");

		}

		else {
			System.out.println("  Success Message did not displayed  ");

		}
		
		
		
		
		
		driver.navigate().to("https://ultimateqa.com/automation/");

	}

}
