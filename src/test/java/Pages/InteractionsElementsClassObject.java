package Pages;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import UtilitiesFile.HighLighter;

public class InteractionsElementsClassObject {
	WebDriver driver = null;

	WebElement element = null;

	By IntElementsTab = By.xpath("//a[contains(text(),'Interactions with simple elements')]");

	By validatingInteractionsElementsPg = By.xpath("//span[contains(text(),'Click button using ClassName')]");

	By clickBtnUsingID = By.xpath("//a[@id='idExample']");

	By linkedInWindow = By.xpath("(//a[@rel='nofollow noreferrer noopener'])[3]");

	By twitterWindow = By.xpath("(//a[@rel='nofollow noreferrer noopener'])[2]");

	By RadioBtnList = By.xpath("//*[@type='radio' and @name = 'gender']");

	By checkBoxList = By.xpath("//*[@type='checkbox' and @name = 'vehicle']");

	By AutomationExercisesTab = By.xpath(" //ul[@id='top-menu']//a[contains(text(),'Automation Exercises')]");

	By InteractionsElementsTab = By.xpath(" //a[contains(text(),'Interactions with simple elements')]");

	By vehicleMakeList = By.xpath(" //div[@class='et_pb_blurb_description']//select");

	By tabList = By.xpath("//*[@href='#']");
	
	By HighLightElem1 = By.xpath("(//span[text()='Highlight me'])[1]");
	
	By HighLightElem2 = By.xpath("(//span[text()='Highlight me'])[2]");
	
	By HighLightElem3 = By.xpath("(//span[text()='Highlight me'])[3]");

	public InteractionsElementsClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void InteractionsElementsTab() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(IntElementsTab).click();
		TimeUnit.SECONDS.sleep(3);
		ScreenShotClassObject.captureScreenShots(driver, "Interactions Wth Simple Elements Page ");

	}

	public void validatingInteractionsElementsPage() {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(validatingInteractionsElementsPg));

		WebElement validate = driver.findElement(validatingInteractionsElementsPg);

		if (validate.getText().equalsIgnoreCase("Click button using ClassName")) {
			System.out.println(
					"PASS--Expected Message ---'Interactions with Simple Elements Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Interactions with Simple Elements Page Did not displayed  ");

		}

	}

	public void clickButtons() throws InterruptedException {
		Actions builder = new Actions(driver);

		builder.moveToElement(driver.findElement(clickBtnUsingID)).build().perform();

		driver.findElement(clickBtnUsingID).click();
		TimeUnit.SECONDS.sleep(3);

		builder.moveToElement(driver.findElement(linkedInWindow)).build().perform();

		driver.findElement(linkedInWindow).click();
		TimeUnit.SECONDS.sleep(3);

		builder.moveToElement(driver.findElement(twitterWindow)).build().perform();

		driver.findElement(twitterWindow).click();
		TimeUnit.SECONDS.sleep(3);

		String parent = driver.getWindowHandle();

		System.out.println("Parent Window id is===>  " + parent);

		Set<String> allWindows = driver.getWindowHandles();

		int count = allWindows.size();

		System.out.println("Total Window ===> " + count);

		for (String child : allWindows)

		{
			if (!parent.equalsIgnoreCase(child)) {
				driver.switchTo().window(child);
				System.out.println("Child Window title is===>  " + driver.getTitle());
				TimeUnit.SECONDS.sleep(3);
				driver.close();

			}
		}

		driver.switchTo().window(parent);
		System.out.println("Parent Window title is ====>" + driver.getTitle());

	}

	public void genderRadioButtons(String male, String female, String other) throws InterruptedException

	{
		TimeUnit.SECONDS.sleep(3);

		driver.findElement(AutomationExercisesTab).click();

		TimeUnit.SECONDS.sleep(2);
		driver.findElement(InteractionsElementsTab).click();

		List<WebElement> radioBtns = driver.findElements(RadioBtnList);

		// WebElement maleChk = driver.findElement(MaleBox);

		for (WebElement elem : radioBtns)

		{
			String values = elem.getAttribute("value");
			System.out.println("Values from radio buttons are =======" + values);

			if (values.equalsIgnoreCase("female") && female.contentEquals("Female"))

			{

				elem.click();
				System.out.println("Gender Selected : Female  ");
				TimeUnit.SECONDS.sleep(3);
				break;

			}

			else if (values.equalsIgnoreCase("other") && other.contentEquals("Other"))

			{

				elem.click();
				System.out.println("Gender Selected :  Other  ");
				TimeUnit.SECONDS.sleep(3);
				break;

			}

			else if (values.equalsIgnoreCase("male") && male.contentEquals("Male"))

			{
				elem.click();
				System.out.println("Gender Selected : Male  ");
				TimeUnit.SECONDS.sleep(3);
				break;

			}

			else {

				System.out.println("Gender NOT Selected");

			}

		}

	}

	public void vehicleCheckBoxs(String bike, String car) throws InterruptedException

	{
		List<WebElement> vehCheckBoxs = driver.findElements(checkBoxList);

		// WebElement maleChk = driver.findElement(MaleBox);

		for (WebElement elemt : vehCheckBoxs)

		{
			String values = elemt.getAttribute("value");
			System.out.println("Values from radio buttons are =======" + values);

			if (values.equalsIgnoreCase("Bike") && bike.contentEquals("Bike"))

			{

				elemt.click();
				System.out.println("BIKE  IS  SELECTED ");
				TimeUnit.SECONDS.sleep(3);
				break;

			}

			else if (values.equalsIgnoreCase("Car") && car.contentEquals("Car"))

			{

				elemt.click();
				System.out.println("CAR IS  SELECTED  ");
				TimeUnit.SECONDS.sleep(3);
				break;

			}

			else {

				System.out.println("Vehicle NOT Selected");

			}

		}

	}

	public void vehicleMake(String VehMake) throws InterruptedException 
	{

		WebElement VeMak = driver.findElement(vehicleMakeList);

		if (VeMak.getText().contains(VehMake)) 
		{

			VeMak.sendKeys(VehMake);
			System.out.println("Vehicle Make Selected ====>" + VehMake);
			TimeUnit.SECONDS.sleep(3);

		}		
	}
	
	
	public void HightLightElements() throws InterruptedException 
	{
	  WebElement HighLightMe1 = driver.findElement(HighLightElem1);
	  
	  WebElement HighLightMe2 = driver.findElement(HighLightElem2);
	  WebElement HighLightMe3 = driver.findElement(HighLightElem3);
	  
	  JavascriptExecutor js = (JavascriptExecutor) driver;
		
		 js.executeScript("window.scrollBy(0,1400)","");
		 TimeUnit.SECONDS.sleep(5);
		
	  HighLighter.highLightElement(driver, HighLightMe1);
	  
	  TimeUnit.SECONDS.sleep(5);
	  
	  HighLighter.highLightElement(driver, HighLightMe2);
	  
	  TimeUnit.SECONDS.sleep(5);
		
	  HighLighter.highLightElement(driver, HighLightMe3);
	  TimeUnit.SECONDS.sleep(5);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
