package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
 
public class LandingPageClassObject {
	
	WebDriver driver = null;

	WebElement element = null;

    By fakeLandingPageTab = By.xpath("//a[contains(text(),'Fake Landing Page')]");
	
	By validatingFakeLandingPage = By.xpath("//h1[contains(text(),'Learn to Code Websites, Apps & Games')]");
	
	
	public LandingPageClassObject(WebDriver driver) {

		this.driver = driver;
	}
	
	
	
	

	public void LandingPageTab() throws InterruptedException 
	{	
		TimeUnit.SECONDS.sleep(3);	
		driver.findElement(fakeLandingPageTab).click();
		TimeUnit.SECONDS.sleep(3);	
		
		
		
	}
	
	public void validatingLandingPage() 
	{
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(validatingFakeLandingPage));		
		
		
		WebElement validate = driver.findElement(validatingFakeLandingPage);
		
		ScreenShotClassObject.captureScreenShots(driver, "Landing Page ");
		
		

		if (validate.getText().equalsIgnoreCase("Learn to Code Websites, Apps & Games")) 
		{
			System.out.println(
					"PASS--Expected Message ---'View Course Page'-- is displayed as expected");
		}

		else {
			System.out.println("  View Course Page Did not displayed  ");

		}
	
		
	
	}	
	
	public void scrollPage() throws InterruptedException 
	{	
		TimeUnit.SECONDS.sleep(3);	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("window.scrollBy(0,1500)","");
		 TimeUnit.SECONDS.sleep(5);
		 js.executeScript("window.scrollBy(0,-1500)","" );
		//js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		 TimeUnit.SECONDS.sleep(5);
		 driver.navigate().back();
		 
	
	}	
	
	
}
