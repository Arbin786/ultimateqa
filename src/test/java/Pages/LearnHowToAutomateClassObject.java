package Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnHowToAutomateClassObject {
	
	WebDriver driver = null;

	WebElement element = null;

    By learnHowToAutomate = By.xpath("//a[contains(text(),'Learn how to automate an application that evolves')]");
	
	By validatinglearnHowToAutomate = By.xpath("//h1[@class='entry-title main_title']");
	
	By Sprnt1 = By.xpath("//input[@name='firstname']");
	
	By Sprnt1Submit = By.xpath("//input[@id='submitForm']");
	
	By GoToSprnt2 = By.xpath("//a[contains(text(),'Go to the next sprint')]");
	
	By Itr2FName = By.xpath("//input[@name='firstname']");
	
	By Itr2LName = By.xpath("//input[@name='lastname']");
	
	By Iterat2Submit = By.xpath("//div[@id='left-area']//input[3]");
	
	By GoToSprnt3 = By.xpath("//a[contains(text(),'Go to sprint 3')]");
	
	By allRadioBtn = By.xpath("//*[@type='radio' and @name = 'gender']");
	
	By MaleBox     = By.xpath("//div[@id='left-area']//input[1]");
	
    By Itr3FName = By.xpath("//input[@name='firstname']");
	
	By Itr3LName = By.xpath("//input[@name='lastname']");
	
	By Iterat3Submit = By.xpath("//input[6]");


	
	
    By GoToSprnt4 = By.xpath("//a[contains(text(),'Go to sprint 4')]");
	
	By allRadioBtn4 = By.xpath("//input[contains(@id,'radio1-')]");	
	
	
    By Itr4FName = By.xpath("//input[@id='f1']");
	
	By Itr4LName = By.xpath("//input[@id='l1']");
	
	By Iterat4Submit = By.xpath("//input[@id='submit1']");
	
	
    
	
	By allRadioBtn4E = By.xpath("//input[contains(@id,'radio2-')]");	
	
	
    By Itr4FNameE = By.xpath("//input[@id='f2']");
	
	By Itr4LNameE = By.xpath("//input[@id='l2']"); 
	
	By Iterat4SubmitE = By.xpath("//input[@id='submit2']");
	
	By MaleBoxE     = By.xpath("//input[@id='radio2-m']");
	
	
	By navigateToAutoExeTab     = By.xpath("//ul[@id='top-menu']//a[contains(text(),'Automation Exercises')]");
	
	
	
	
	
	
	public LearnHowToAutomateClassObject(WebDriver driver) {

		this.driver = driver;
	}
	
	
	public void learnHowToAutomateTab() throws InterruptedException 
	{
		TimeUnit.SECONDS.sleep(3);	
		driver.findElement(learnHowToAutomate).click();
		TimeUnit.SECONDS.sleep(3);	
			
	}
	
	public void validatinglearnHowToAutomatePage() {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(validatinglearnHowToAutomate));

		WebElement validate = driver.findElement(validatinglearnHowToAutomate);

		ScreenShotClassObject.captureScreenShots(driver, "Sample Application Lifecycle Page ");

		if (validate.getText().equalsIgnoreCase("Sample Application Lifecycle � Sprint 1")) {
			System.out.println("PASS--Expected Message ---'Sample Application Lifecycle Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Sample Application Lifecycle Page Did not displayed  ");

		}

	}

	public void Sprint1(String FirstName) throws InterruptedException 
	{
		
		driver.findElement(Sprnt1).sendKeys(FirstName);
		TimeUnit.SECONDS.sleep(3);	
		driver.findElement(Sprnt1).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(3);
		driver.navigate().back();
			
	}
	
	
	
	public void GoToSprint2Iteration2(String fName2, String LName2) throws InterruptedException 
	
	{
		TimeUnit.SECONDS.sleep(3);			
		driver.findElement(GoToSprnt2).sendKeys(Keys.ENTER);		
		
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr2FName).clear();
		driver.findElement(Itr2FName).sendKeys(fName2);
		TimeUnit.SECONDS.sleep(3);	
		driver.findElement(Itr2LName).clear();
		driver.findElement(Itr2LName).sendKeys(LName2);
		driver.findElement(Iterat2Submit).click();
		TimeUnit.SECONDS.sleep(3);
		driver.navigate().back();
		TimeUnit.SECONDS.sleep(3);
	}
	
	
	public void GoToSprint3Iteration3(String male, String female, String other) throws InterruptedException {

		driver.findElement(GoToSprnt3).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(3);

		List<WebElement> radioBtns = driver.findElements(allRadioBtn);

		WebElement maleChk = driver.findElement(MaleBox);

		for (WebElement ele : radioBtns)

		{
			String values = ele.getAttribute("value");
			System.out.println("Values from radio buttons are =======" + values);

			if (values.equalsIgnoreCase("female") && female.contentEquals("Female"))

			{

				ele.click();
				System.out.println("Gender Selected : Female  ");
				break;

			}

			else if (values.equalsIgnoreCase("other") && other.contentEquals("Other"))

			{

				ele.click();
				System.out.println("Gender Selected :  Other  ");
				break;

			}

			else if (values.equalsIgnoreCase("male") && male.contentEquals("male") && maleChk.isSelected())

			{

				System.out.println("Gender Selected : Male  ");
				break;

			}

			else 
			{

				System.out.println("Gender NOT Selected");

			}

		}
	}
		
		public void GoToSprint3Iteration3FullName(String fName3, String lName3) throws InterruptedException {
		
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr3FName).clear();
		driver.findElement(Itr3FName).sendKeys(fName3);
		TimeUnit.SECONDS.sleep(3);	
		driver.findElement(Itr3LName).clear();
		driver.findElement(Itr3LName).sendKeys(lName3);
		TimeUnit.SECONDS.sleep(2);	
		driver.findElement(Iterat3Submit).click();
		TimeUnit.SECONDS.sleep(2);
		driver.navigate().back();
		TimeUnit.SECONDS.sleep(3);
		
		
		}
		
		
		
	public void GoToSprint4Iteration4(String male1, String female1, String other1) throws InterruptedException {

		driver.findElement(GoToSprnt4).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(3);

		List<WebElement> radioBtn4 = driver.findElements(allRadioBtn4);

	//	WebElement maleChk = driver.findElement(MaleBox);

		for (WebElement ele : radioBtn4)

		{
			String values = ele.getAttribute("value");
			System.out.println("Values from radio buttons are =======" + values);

			if (values.equalsIgnoreCase("female") && female1.contentEquals("Female"))

			{

				ele.click();
				System.out.println("Gender Selected : Female  ");
				break;

			}

			else if (values.equalsIgnoreCase("other") && other1.contentEquals("Other"))

			{

				ele.click();
				System.out.println("Gender Selected :  Other  ");
				break;

			}

			else if (values.equalsIgnoreCase("male") && male1.contentEquals("Male"))

			{
				ele.click();
				System.out.println("Gender Selected : Male  ");
				break;

			}

			else {

				System.out.println("Gender NOT Selected");

			}

		}
	}

	public void GoToSprint4Iteration4FullName(String fName4, String lName4) throws InterruptedException {

		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr4FName).clear();
		driver.findElement(Itr4FName).sendKeys(fName4);
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr4LName).clear();
		driver.findElement(Itr4LName).sendKeys(lName4);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(Iterat4Submit).click();
		TimeUnit.SECONDS.sleep(2);
		driver.navigate().back();
		TimeUnit.SECONDS.sleep(3);

	}
		
	
	public void Sprint4Itert4EME(String male2, String female2, String other2) throws InterruptedException {

		
		List<WebElement> radioBtn4E = driver.findElements(allRadioBtn4E);

		//WebElement maleChkE = driver.findElement(MaleBoxE);

		for (WebElement ele : radioBtn4E)

		{
			String values = ele.getAttribute("value");
			System.out.println("Values from radio buttons are =======" + values);

			if (values.equalsIgnoreCase("female") && female2.contentEquals("Female"))

			{

				ele.click();
				System.out.println("Gender Selected : Female  ");
				break;

			}

			else if (values.equalsIgnoreCase("other") && other2.contentEquals("Other"))

			{

				ele.click();
				System.out.println("Gender Selected :  Other  ");
				break;

			}

			else if (values.equalsIgnoreCase("male") && male2.contentEquals("Male"))

			{
				ele.click();
				System.out.println("Gender Selected : Male  ");
				break;

			}

			else {

				System.out.println("Gender NOT Selected");

			}

		}
	}

	public void Sprint4Itert4EMEFullName(String fName4E, String lName4E) throws InterruptedException {

		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr4FNameE).clear();
		driver.findElement(Itr4FNameE).sendKeys(fName4E);
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(Itr4LNameE).clear();
		driver.findElement(Itr4LNameE).sendKeys(lName4E);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(Iterat4SubmitE).click();
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(navigateToAutoExeTab).click();
		TimeUnit.SECONDS.sleep(3);

	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
	

	
	
	
	
	
	
	
	
	
	
	
	


