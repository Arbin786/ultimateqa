package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import UtilitiesFile.HighLighter;



public class LoginAutomationClassObject {
	
	WebDriver driver = null;

	WebElement element = null;

	By SignInPage = By.xpath("//h1[@class='page__heading']");
	
	By LoginAutomation = By.xpath("//a[contains(text(),'Login automation')]");
	
	By CreateNewAcc = By.xpath("//a[contains(text(),'Create a new account')]");
	
	By firstName = By.xpath("//input[@id='user[first_name]']");
	
	By lastName = By.xpath("//input[@id='user[last_name]']");
	
	By Email = By.xpath("//input[@id='user[email]']");
	
	By Password = By.xpath("//input[@id='user[password]']");
	
	
	By AgreeChkBox = By.xpath("//input[@id='user[terms]']");	
	
	
	By SignIn = By.xpath("//li[@class='header__nav-item header__nav-sign-in']//a[contains(text(),'Sign In')]");
	
	By loginEmail = By.xpath("//input[@id='user[email]']");
	
	By loginPassword = By.xpath("//input[@id='user[password]']");
	
	By rememberMeChkBox = By.xpath("//input[@id='user[remember_me]']");
	
	By AutomationPracticePage = By.xpath("//span[@id='Automation_Practice']");
	
	
	

	public LoginAutomationClassObject(WebDriver driver) {

		this.driver = driver;
	}
	
	
	public void validatingAutomationPracticePage() {
		WebElement validate = driver.findElement(AutomationPracticePage);

		if (validate.getText().contains("Automation Practice")) {
			System.out.println(
					"PASS--Expected Message ---HOORAY'Automation Practice Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Automation Practice Page Did not displayed  ");

		}

	}
	
	public void LoginAutomationTab() throws InterruptedException 
	{		
					
			//driver.manage().timeouts().pageLoadTimeout(15, SECONDS);
			 
			driver.findElement(LoginAutomation).click();
			TimeUnit.SECONDS.sleep(6);
		    ScreenShotClassObject.captureScreenShots(driver, "Sign In Page");
		
	}
	
	
	
	public void validatingSignInPage() {
		WebElement validate = driver.findElement(SignInPage);

		if (validate.getText().contains("Welcome Back!")) {
			System.out.println(
					"PASS--Expected Message ---'SignIn Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Sign In Page Did not displayed  ");

		}

	}
	
	public void CreateNewAccount() throws InterruptedException 
	{		
					
			//driver.manage().timeouts().pageLoadTimeout(15, SECONDS);
		
		    			 
			driver.findElement(CreateNewAcc).click();
			TimeUnit.SECONDS.sleep(6);
		    ScreenShotClassObject.captureScreenShots(driver, "Create New Account  Page");
		
	}
	
	
	
	public void fillOutNewAccInfo() throws InterruptedException 
	{
		
		
		
	// Creating Obj of Xls_Reader  	
	Xls_Reader reader = new Xls_Reader(".\\ExcelFiles\\LogIn Info.xlsx");
	

	int rowCount = reader.getRowCount("UserInfo");

	// Parameterization :
	for (int rowNum = 2; rowNum <= rowCount; rowNum++) 
	{
		System.out.println("=====================");
		String firstNameExcel = reader.getCellData("UserInfo", "First Name", rowNum);

		System.out.println(firstNameExcel);

		String lastNameExcel = reader.getCellData("UserInfo", "Last Name", rowNum);

		System.out.println(lastNameExcel);

		String EmailExcel = reader.getCellData("UserInfo", "Email", rowNum);

		System.out.println(EmailExcel);

		String passwordExcel = reader.getCellData("UserInfo", "Password", rowNum);

		System.out.println(passwordExcel);
		
		System.out.println("=====================");
		
		driver.findElement(firstName).clear();

		driver.findElement(firstName).sendKeys(firstNameExcel);

		TimeUnit.SECONDS.sleep(2);

		driver.findElement(lastName).clear();
		
		driver.findElement(lastName).sendKeys(lastNameExcel);

		TimeUnit.SECONDS.sleep(2);

		driver.findElement(Email).clear();
		
		driver.findElement(Email).sendKeys(EmailExcel);
		

		TimeUnit.SECONDS.sleep(2);

		driver.findElement(Password).clear();
		driver.findElement(Password).sendKeys(passwordExcel);

		TimeUnit.SECONDS.sleep(2);
		
		driver.findElement(AgreeChkBox).click();
		
		TimeUnit.SECONDS.sleep(2);
		
		driver.findElement(SignIn).click();	
		
		TimeUnit.SECONDS.sleep(2);
		
		break;
		
		
		
		
	}

}
	
	
	public void logIn(String email , String pass) throws InterruptedException 
	{	
		  WebElement HighLightMe1 = driver.findElement(loginEmail);
		  WebElement HighLightMe3 = driver.findElement(Password);
		
		 
		
		
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(loginEmail).clear();
		driver.findElement(loginEmail).click();
		 HighLighter.highLightElement(driver, HighLightMe1);
		driver.findElement(loginEmail).sendKeys(email);	
		
		TimeUnit.SECONDS.sleep(2);
		
		driver.findElement(Password).clear();
		 HighLighter.highLightElement(driver, HighLightMe3);
		driver.findElement(Password).sendKeys(pass);	
		
		TimeUnit.SECONDS.sleep(2);
		
		driver.findElement(rememberMeChkBox).click();
		
		TimeUnit.SECONDS.sleep(2);
		
		
		
		driver.navigate().to("https://ultimateqa.com/automation/");
	 
		
	} 
	
}	
