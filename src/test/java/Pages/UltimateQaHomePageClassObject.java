package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UltimateQaHomePageClassObject {
	
	WebDriver driver = null;

	WebElement element = null;

	By AutomationExe = By.xpath("//ul[@id='top-menu']//a[contains(text(),'Automation Exercises')]");
	
	
	

	public UltimateQaHomePageClassObject(WebDriver driver) {

		this.driver = driver;
	}
	
	
	
	

	public void AutomationExercisesTab() throws InterruptedException 
	{
		ScreenShotClassObject.captureScreenShots(driver, "Ultimate QA Home  Page");
		//driver.manage().timeouts().pageLoadTimeout(15, SECONDS);
		 
		driver.findElement(AutomationExe).click();
		
		Thread.sleep(1000);

	}

}
