package TestNG;

import org.testng.annotations.Test;

import Pages.AutoExeBigPageTabClassObject;
import Pages.FillOutFormClassObject;
import Pages.InteractionsElementsClassObject;
import Pages.LandingPageClassObject;
import Pages.LearnHowToAutomateClassObject;
import Pages.LoginAutomationClassObject;
import Pages.PricingPageClassObject;
import Pages.UltimateQaHomePageClassObject;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;

public class UltimateQATestNG {

	private static final String priority = null;
	static WebDriver driver = null;

	@BeforeSuite
	public void beforeSuite() {

		System.out.println("Before Suite method");
	}

	@BeforeTest

	public void setUptest() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}
	public static void navigateToUltimateQA() {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.ultimateqa.com/");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
	
	
	
	
	

	@Test(priority = 0)
	public void UltimateQAPage () throws InterruptedException 
	{
		// navigate to ultimateqa.com
		
		navigateToUltimateQA();
		
		
		// Creating an object for UltimateQaHomePage & Selecting AutomationExercisesTab

					UltimateQaHomePageClassObject obj1 = new UltimateQaHomePageClassObject(driver);					
					obj1.AutomationExercisesTab();	
		
	}

	
	@Test(priority = 1)
	public void ULoginAutomationPage () throws InterruptedException 
	{
		// Creating an object for LoginAutomationClassObject,
		
					LoginAutomationClassObject obj2 = new LoginAutomationClassObject (driver);
					
					obj2.validatingAutomationPracticePage();
					obj2.LoginAutomationTab();
					obj2.validatingSignInPage();
					obj2.CreateNewAccount();
					obj2.fillOutNewAccInfo();			
					obj2.logIn("Arbinpradhan@gmail.com", "LoveisBlind2020");	
	}
	
	@Test(priority = 2)
	public void AutoExeBigPage () throws InterruptedException 
	{
		// Creating an object for AutoExeBigPageTab,			
		 
					AutoExeBigPageTabClassObject obj3 = new AutoExeBigPageTabClassObject(driver);
					
					obj3.BigPageManyElementTab();
					obj3.validatingBigPageManyEle();
					obj3.faceBookLnk();
					obj3.RandomStuffNameEmail("Binod Shrestha","Arbinpradhan@gmail.com");
					obj3.RandomStuffMessage("How Many Month QA Class will Run ");
					obj3.RandomStuffSumBox();	
	}
	
	
	@Test(priority = 3)
	public void LandingPage () throws InterruptedException 
	{
		// Creating an object for LandingPageClassObject,
		
					LandingPageClassObject obj4 = new LandingPageClassObject (driver);
					
					obj4.LandingPageTab();
					obj4.validatingLandingPage();
					obj4.scrollPage();	
	}
	
	@Test(priority = 4)
	public void PricingPage () throws InterruptedException 
	{
		// Creating an object for LandingPageClassObject,
		
					PricingPageClassObject obj5 = new PricingPageClassObject (driver);
					
					obj5.PricingPageTab();
					obj5.validatingPricingPage();
					obj5.scrollPage();	
	}
	
	
	@Test(priority = 5)
	public void FillOutFormPage () throws InterruptedException 
	{
		// Creating an object for FillOutFormClassObject
		
					FillOutFormClassObject obj6 = new FillOutFormClassObject(driver);
					
					obj6.fillOutFormsPageTab();
					obj6.fillOutFormsName1("Ryan Magar", "I am looking of QA  trainging, Please call me my number is 454-584-5855");
					obj6.fillOutFormsName2("Maya Devi", "What is the Duration of QA Class.");
					obj6.fillOutFormsNumber();		
	}
	
	@Test(priority = 6)
	public void LearnHowToAutomatePage () throws InterruptedException 
	{
		// Creating an object for LearnHowToAutomateClassObject
		
					LearnHowToAutomateClassObject obj7 = new LearnHowToAutomateClassObject(driver);
					
					obj7.learnHowToAutomateTab();
					obj7.validatinglearnHowToAutomatePage();
					obj7.Sprint1("Harry");
					obj7.GoToSprint2Iteration2("Bob", "Marley");
					obj7.GoToSprint3Iteration3("","Female","");
					obj7.GoToSprint3Iteration3FullName("Nabin","Dangol");
					obj7.GoToSprint4Iteration4("", "Female", "");
					obj7.GoToSprint4Iteration4FullName("Sam", "Kumar");
					obj7.Sprint4Itert4EME("", "", "Other");
					obj7.Sprint4Itert4EMEFullName("Arun", "Prasad");
					
	}
	

	
	@Test(priority = 7)
	public void InteractionsElementsPage () throws InterruptedException 
	{
		// Creating an object for InteractionsElementsClassObject
		
					InteractionsElementsClassObject obj8 = new InteractionsElementsClassObject(driver);
					
					obj8.InteractionsElementsTab();
					obj8.validatingInteractionsElementsPage();
					obj8.clickButtons();
					obj8.genderRadioButtons("Male", "", "");
					obj8.vehicleCheckBoxs("Bike", "");
					obj8.vehicleMake("Saab");
					obj8.HightLightElements();
		
	}
	
	
	
	@AfterTest

	public static void terminateTest() throws InterruptedException 
	{

		// Close the browser
		Thread.sleep(10000);
		 driver.close();
		 driver.quit();

	}

	@AfterMethod
	@AfterSuite
	public void tearDown() 
	{

		
		
		
	}

	
	
	
	
	
	
	
	
	
	
	

}
